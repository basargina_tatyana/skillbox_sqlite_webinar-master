-- Задачи День 1:

-- Покажите  фамилию и имя клиентов из города Прага ?
SELECT LastName,
       FirstName
  FROM customers
 WHERE City = 'Prague';

-- Покажите  фамилию и имя клиентов у которых имя начинается букву M ? Содержит символ "ch"?
SELECT LastName,
       FirstName
  FROM customers
 WHERE FirstName LIKE 'M%' OR FirstName LIKE '%ch%';

-- Покажите название и размер музыкальных треков в Мегабайтах ?
SELECT Name,
       Bytes/1024/1024
  FROM tracks;

-- Покажите  фамилию и имя сотрудников кампании нанятых в 2002 году из города Калгари ?
SELECT LastName,
       FirstName
  FROM employees
 WHERE strftime('%Y', HireDate) = '2002' AND 
       City = 'Calgary';
  
-- Покажите  фамилию и имя сотрудников кампании нанятых в возрасте 40 лет и выше?
SELECT LastName,
       FirstName
  FROM employees
 WHERE date(HireDate, 'start of year') - date(BirthDate, 'start of year') >= 40;

-- Покажите покупателей-амерканцев без факса ?
SELECT FirstName || ' ' || LastName AS Employee
  FROM customers
 WHERE Country = 'USA' AND 
       Fax IS NULL;

-- Покажите канадские города в которые сделаны продажи в августе и сентябре месяце? 
SELECT ShipCity
  FROM sales
 WHERE ShipCountry = 'Canada' AND 
       strftime('%m', SalesDate) IN ('08', '09');

-- Покажите  почтовые адреса клиентов из домена gmail.com ?
SELECT Email
  FROM customers
 WHERE Email LIKE '%gmail.com';

-- Покажите сотрудников  которые работают в кампании уже 18 лет и более ?
SELECT FirstName || ' ' || LastName AS Employee
  FROM employees
 WHERE date(CURRENT_DATE, 'start of year') - date(HireDate, 'start of year') >= 18;

-- Покажите  в алфавитном порядке все должности в кампании  ?
SELECT DISTINCT Title
  FROM employees
 ORDER BY Title;
 
-- Покажите  в алфавитном порядке Фамилию, Имя и год рождения покупателей  ?
--         Примечание: Вам поможет документация ниже
--         https://www.sqlitetutorial.net/sqlite-date-functions/sqlite-date-function/
SELECT LastName,
       FirstName,
       strftime('%Y', date(CURRENT_DATE, '-' || Age || ' year') ) AS YearBirthDate
  FROM customers
 ORDER BY LastName,
          FirstName,
          BirthDate;

-- Сколько секунд длится самая короткая песня ?
-- Покажите название и длительность в секундах самой короткой песни
SELECT Name,
       MIN(Milliseconds * 0.001) AS Sec
  FROM tracks;
  
-- Покажите средний возраст клиента для каждой страны ?
SELECT Country,
       AVG(Age) AS 'AVG age'
  FROM customers
 GROUP BY Country;

-- Покажите Фамилии работников нанятых в октябре?
SELECT LastName
  FROM employees
 WHERE strftime('%m', HireDate) = '10';
  
-- Покажите фамилию самого старого сотрудника
SELECT LastName
  FROM employees
 WHERE strftime('%Y', BirthDate) = (
                                       SELECT MIN(strftime('%Y', BirthDate) ) 
                                         FROM employees
                                   );

  
-- Посчитайте общую сумму продаж в америку  в 1 квартале 2012 года? Решить 2-мя  способами Джойнами и Подзапросами 

-- Подзапросами 
SELECT ROUND(SUM( (
                      SELECT SUM(si.UnitPrice) 
                        FROM sales_items si
                       WHERE si.SalesId = s.SalesId
                  )
             ), 2) AS SumUnitPrice
  FROM sales s
 WHERE s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';

-- Джойнами 
SELECT Round(SUM(si.UnitPrice), 2) AS SumUnitPrice
  FROM sales s
       INNER JOIN
       sales_items si ON s.SalesId = si.SalesId
 WHERE s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';

       
SELECT ROUND(SUM(si.UnitPrice), 2) AS SumUnitPrice
  FROM sales s,
       sales_items si
 WHERE si.SalesId = s.SalesId AND 
       s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';