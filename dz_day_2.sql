--	Задачи День 2:

-- 1	Посчитайте общую сумму продаж в США в 1 квартале 2012 года? Решить 2-мя  способами Джойнами и Подзапросами 
-- Подзапросами 
SELECT ROUND(SUM( (
                      SELECT SUM(si.UnitPrice) 
                        FROM sales_items si
                       WHERE si.SalesId = s.SalesId
                  )
             ), 2) AS SumUnitPrice
  FROM sales s
 WHERE s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';

-- Джойнами 
SELECT Round(SUM(si.UnitPrice), 2) AS SumUnitPrice
  FROM sales s
       INNER JOIN
       sales_items si ON s.SalesId = si.SalesId
 WHERE s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';

       
SELECT ROUND(SUM(si.UnitPrice), 2) AS SumUnitPrice
  FROM sales s,
       sales_items si
 WHERE si.SalesId = s.SalesId AND 
       s.ShipCountry = 'USA' AND 
       strftime('%m', s.SalesDate) IN ('01', '02', '03') AND 
       strftime('%Y', s.SalesDate) = '2012';

-- 2	Покажите имена клиентов, которых нет среди работников.
-- 	Решить 3-мя способами: подзапросами, джойнами и логическим вычитанием.

-- подзапросами
SELECT DISTINCT FirstName
  FROM customers
 WHERE FirstName NOT IN (
           SELECT FirstName
             FROM employees
       );

-- джойнами
SELECT DISTINCT c.FirstName
  FROM customers c LEFT JOIN employees e
  ON c.FirstName = e.FirstName;

-- логическим вычитанием
SELECT FirstName
  FROM customers
EXCEPT
SELECT FirstName
  FROM employees;

-- 3	Теоретический вопрос
-- 	Вернет ли данный запрос  одинаковый результат?  Да или НЕТ. 
-- 	Если  ДА. Объяснить почему.
-- 	Если НЕТ. Объяснить почему. Какой  запрос вернет больше строк ?
-- 
-- 	select *
-- 	from T1 LEFT JOIN T2
-- 	ON T1.column1=T2.column1
-- 	 where   T1.column1=0
 	
-- 	 select *
-- 	 from T1 LEFT JOIN T2
-- 		ON T1.column1=T2.column1 and   T1.column1=0

/* Ответ:
Нет. При соединении таблиц дополнительное условие and не повлияет на результат. 
Выражение  where отбросит не соответстующие условию строки. Тестировала на запросах ниже*/

SELECT DISTINCT c.FirstName
  FROM customers c
       LEFT JOIN
       employees e ON c.FirstName = e.FirstName AND 
                      c.FirstName = 'Helena';

  
SELECT DISTINCT c.FirstName
  FROM customers c
       LEFT JOIN
       employees e ON c.FirstName = e.FirstName
 WHERE c.FirstName = 'Helena';


-- 4	Посчитайте количество треков в каждом альбоме. В результате должно быть:  имя альбома и кол-во треков.
-- 	Решить  2-мя способами: подзапросом и джойнами 

-- подзапросом
SELECT a.Title AS [имя альбома],
       (
           SELECT COUNT(t.AlbumId) 
             FROM tracks t
            WHERE t.AlbumId = a.AlbumId
       )
       AS [кол-во треков]
  FROM albums a
  ORDER BY a.Title;

-- джойнами
SELECT a.Title AS [имя альбома],
       COUNT(t.AlbumId) AS [кол-во треков]
  FROM albums a
       LEFT JOIN
       tracks t ON t.AlbumId = a.AlbumId
 GROUP BY a.Title
 ORDER BY a.Title;

-- 5	Покажите фамилию и имя покупателей немцев сделавших заказы в 2009 году, товары которых были отгружены в 
--      город Берлин?
SELECT c.LastName AS фамилия,
       c.FirstName AS имя
  FROM customers c
       JOIN
       sales s ON c.CustomerId = s.CustomerId
 WHERE c.Country = 'Germany' AND 
       s.ShipCity = 'Berlin' AND 
       strftime('%Y', s.SalesDate) = '2009'
 GROUP BY c.LastName,
          c.FirstName;


-- ИЛИ
SELECT DISTINCT c.LastName AS фамилия,
                c.FirstName AS имя
  FROM customers c
       JOIN
       sales s ON c.CustomerId = s.CustomerId
 WHERE c.Country = 'Germany' AND 
       s.ShipCity = 'Berlin' AND 
       strftime('%Y', s.SalesDate) = '2009';

-- подзапросом 
SELECT c.LastName AS [фамилия], c.FirstName AS  [имя]
  FROM customers c
 WHERE c.CustomerId IN (
           SELECT s.CustomerId
             FROM sales s
            WHERE s.ShipCity = 'Berlin' AND 
                  strftime('%Y', s.SalesDate) = '2009'
       )
AND 
       c.Country = 'Germany';

-- 6	Покажите фамилии  клиентов которые  купили больше 30 музыкальных треков ?
-- 	Решить  задачу ,как минимум, 2-мя способами: джойнами и подзапросами

-- джойнами
SELECT c.LastName AS фамилия
  FROM customers c
       JOIN
       sales s ON c.CustomerId = s.CustomerId
       JOIN
       sales_items si ON si.SalesId = s.SalesId
 GROUP BY c.CustomerId
HAVING COUNT(si.TrackId) > 30
 ORDER BY c.LastName;

-- подзапросами
SELECT c.LastName AS фамилия
  FROM customers c
 WHERE c.CustomerId = (
                          SELECT s.CustomerId
                            FROM sales s
                           WHERE c.CustomerId = s.CustomerId AND 
                                 s.SalesId = (
                                                 SELECT si.SalesId
                                                   FROM sales_items si
                                                  WHERE si.SalesId = s.SalesId
                                             )
                      )
 GROUP BY c.CustomerId
HAVING (
           SELECT COUNT(si.TrackId) 
             FROM sales_items si
            WHERE si.SalesId IN (
                      SELECT s.SalesId
                        FROM sales s
                       WHERE c.CustomerId = s.CustomerId
                  )
       )
>      30
 ORDER BY c.LastName;

-- 7	В базе есть таблица музыкальных треков и жанров 
--      Назовите среднюю стоимстость музыкального трека в каждом жанре?
SELECT g.Name AS [жанр], ROUND(AVG(t.UnitPrice), 2) AS [средняя стоимстость] 
  FROM tracks t
       JOIN
       genres g ON t.GenreId = g.GenreId
GROUP BY g.Name;


-- 8	В базе есть таблица музыкальных треков и жанров. Покажите жанры у которых 
--      средняя стоимость одного трека больше 1-го рубля
SELECT g.Name AS [жанр], ROUND(AVG(t.UnitPrice), 2) AS [средняя стоимстость] 
  FROM tracks t
       JOIN
       genres g ON t.GenreId = g.GenreId
GROUP BY g.Name
HAVING ROUND(AVG(t.UnitPrice), 2) > 1;